import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface School {
    id: number;
}

export const API = 'http://192.168.10.55:8000/api';

@Injectable()
export class SchoolService {
    constructor(private http: HttpClient) {}

    getSchools(state: string, limit: number, page: number): Observable<School> {
        const params = {
            limit: limit.toString(),
            page: page.toString()
        };

        return this.http.get<School>(`${API}/escolas/uf/${state}`, { params });
    }

    getSchoolsJson(state: string, limit: number, page: number): Observable<School> {
        const params = {
            limit: limit.toString(),
            page: page.toString()
        };


        const api = 'http://d3dpk9qvioisv.cloudfront.net';
        return this.http.get<School>(`${api}/escolas/${state.toLowerCase()}.json`, { params });
    }

    saveSchoolLatLong(idEscola: number, uf: string, metadata: any) {
        const body = {
            lat: metadata.lat,
            long: metadata.long,
            status: metadata.status,
            address: metadata.address,
            uf
        };

        return this.http.post(
            `${API}/escolas/${idEscola}/importa-coordenadas`,
            body,
            { responseType: 'text' }
        );
    }
}
