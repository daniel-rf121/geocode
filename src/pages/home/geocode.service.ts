import { Injectable } from '@angular/core';
import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';

export enum GeocodeStatus {
    INVALID_AREA = -2,
    INVALID_LOCATION,
    INVALID_ADDRESS,
    SUCCESS,
}

@Injectable()
export class GeocodeService {    

    constructor(private geocode: NativeGeocoder) {}

    async resolveLatLong(data: any): Promise<{ status: number, lat?: string, long?: string, address?: string }> {
        const { address, valid } = this.buildAddress(data);

        if (!valid) {
            return {
                status: GeocodeStatus.INVALID_ADDRESS,
                address
            };
        }

        try {
            const coordinates: NativeGeocoderForwardResult[] = await this.forwardGeocode(address);
            return {
                status: GeocodeStatus.SUCCESS,
                lat: coordinates[0].latitude,
                long: coordinates[0].longitude,
                address
            };
        } catch (e) {
            return {
                status: GeocodeStatus.INVALID_LOCATION,
                address
            }
        }
    }

    forwardGeocode(address: string) {
        let options: NativeGeocoderOptions = {
            useLocale: false,
            maxResults: 1
        };
        return this.geocode.forwardGeocode(address, options);
    }

    async checkIsInsideState(latitude: number, longitude: number, state: any) {
        const result: NativeGeocoderReverseResult[] = await this.geocode.reverseGeocode(latitude, longitude);

        const administrativeArea = result[0].administrativeArea.toLowerCase();
        const sigla = state.sigla.toLowerCase();
        const nome = state.nome.toLowerCase();

        return administrativeArea === sigla || administrativeArea === nome; 
    }

    private buildAddress(data: any) {
        let fullAddress = '';

        let valid = true;
        if (data.ds_endereco_entidade) {
            let address = this.buildTypeAddress(data);
            if (!address) {
                valid = false;
                address = data.ds_endereco_entidade;
            }
            
            fullAddress += address + ' ';
        }

        if (data.nu_cep) {
            let cep = data.nu_cep;
            /* if (cep.length === 8) {
                cep = cep.substr(0, 5) + '-' + cep.substr(5);
            } */
            fullAddress += cep +  ' ' ;
        }

        fullAddress = fullAddress.trim();
        return { valid, address: `${fullAddress}, ${data.no_municipio} ${data.sg_uf}, Brazil`};
    }

    private buildTypeAddress(data: any) {
        const { ds_endereco_entidade } = data;
        const regex = this.buildRegexTermAddress(data);

        if (regex.test(ds_endereco_entidade)) {
            return ds_endereco_entidade;
        }

        return this.normalizeAddress(data);
    }

    private buildRegexTermAddress(data: any) {
        let terms = ['rua', 'avenida', 'praca'];
        const { sg_uf } = data;

        if (sg_uf === 'DF') {
            const termsAdd = [
                'quadra', 'chacara', 'vila', 'nucleo'
            ]
            terms = terms.concat(termsAdd);
        }

        return new RegExp(`(${terms.join('|')})`, 'ig');
    }

    private normalizeAddress(data: any) {

        const { ds_endereco_entidade } = data;
        const replacements = {
            'Avenida': /^(a|av|ave)(?:\s|\.)/i,
            'Rua': /^r\s/i,
            'Praca': /^pca\s/i
        };

        for (const key of Object.keys(replacements)) {
            if (replacements[key].test(ds_endereco_entidade)) {
                return ds_endereco_entidade.replace(replacements[key], key);
            }
        }

        return '';
    }
}
