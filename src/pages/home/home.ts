import { Component } from '@angular/core';
import { GeocodeService, GeocodeStatus } from './geocode.service';
import { SchoolService } from './school.service';
import { Loading, LoadingController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  currentState: any;

  resultPage: any;

  countError = 0;

  quantityByState = 0;

  page = 1;

  limit = -1;

  private loading: Loading;

  estados = [
    {'nome': 'Acre', 'sigla': 'AC', 'status': ''},
    {'nome': 'Alagoas', 'sigla': 'AL', 'status': ''},
    {'nome': 'Amapá', 'sigla': 'AP', 'status': ''},
    {'nome': 'Amazonas', 'sigla': 'AM', 'status': ''},
    {'nome': 'Bahia', 'sigla': 'BA', 'status': ''},
    {'nome': 'Ceará', 'sigla': 'CE', 'status': ''},
    {'nome': 'Distrito Federal', 'sigla': 'DF', 'status': ''},
    {'nome': 'Espírito Santo', 'sigla': 'ES', 'status': ''},
    {'nome': 'Goiás', 'sigla': 'GO', 'status': ''},
    {'nome': 'Maranhão', 'sigla': 'MA', 'status': ''},
    {'nome': 'Mato Grosso', 'sigla': 'MT', 'status': ''},
    {'nome': 'Mato Grosso do Sul', 'sigla': 'MS', 'status': ''},
    {'nome': 'Minas Gerais', 'sigla': 'MG', 'status': ''},
    {'nome': 'Pará', 'sigla': 'PA', 'status': ''},
    {'nome': 'Paraíba', 'sigla': 'PB', 'status': ''},
    {'nome': 'Paraná', 'sigla': 'PR', 'status': ''},
    {'nome': 'Pernambuco', 'sigla': 'PE', 'status': ''},
    {'nome': 'Piauí', 'sigla': 'PI', 'status': ''},
    {'nome': 'Rio de Janeiro', 'sigla': 'RJ', 'status': ''},
    {'nome': 'Rio Grande do Norte', 'sigla': 'RN', 'status': ''},
    {'nome': 'Rio Grande do Sul', 'sigla': 'RS', 'status': ''},
    {'nome': 'Rondônia', 'sigla': 'RO', 'status': ''},
    {'nome': 'Roraima', 'sigla': 'RR', 'status': ''},
    {'nome': 'Santa Catarina', 'sigla': 'SC', 'status': ''},
    {'nome': 'São Paulo', 'sigla': 'SP', 'status': ''},
    {'nome': 'Sergipe', 'sigla': 'SE', 'status': ''},
    {'nome': 'Tocantins', 'sigla': 'TO', 'status': ''}
  ];

  constructor(
    private schoolService: SchoolService,
    private geocodeService: GeocodeService,
    private loadingCtrl: LoadingController,
    private alertController: AlertController
  ) {}

  async start(estado: any) {
    this.currentState = estado;
    this.quantityByState = 0;
    this.countError = 0;
    this.importLatLong();
  }

  private async importLatLong() {
    // limpando loading anterior
    this.loading && this.loading.dismiss();
    this.loading = null;

    let stopPagination = false;
    if (this.resultPage) {
      stopPagination = this.resultPage.last_page ? this.page > this.resultPage.last_page : true; 
    }

    if (stopPagination) {
      if (this.countError > 0) {
        this.alertRepeatImport();
      }
      
      return;
    }

    const asyncLoop = async (data: any[]) => {
      for (let index = 0; index < data.length; index++) {
        const value = data[index];
        try {
          await this.buildLatLong(value);
        } catch (e) {
          this.countError++;
        }
      }
    };

    const result: any = await this.loadSchools();
    await asyncLoop(result);

    this.page++;
    this.importLatLong();
  }

  private alertRepeatImport() {
    const alert = this.alertController.create({
      title: 'Erro importação',
      message: `Existem ${this.countError} registros houve erro, deseja repetir?`,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'OK',
          handler: () => {
            this.start(this.currentState);
          }
        }
      ]
    });
    alert.present();
  }

  private total() {
    return this.resultPage.total ? this.resultPage.total : this.resultPage.length;
  }

  private async loadSchools() {

    let loading;
    if (this.limit > 0) {
      loading = this.loadingCtrl.create({
        content: `
          Carregando ${this.limit} escolas do estado: ${this.currentState.nome}
        `
      });
    }

    loading && loading.present();
    let result: any;
    try {
      result = await this.schoolService.getSchoolsJson(this.currentState.sigla, this.limit, this.page).toPromise();
      !this.resultPage && (this.resultPage = result);
    } catch (e) {
      throw e;
    } finally {
      loading && loading.dismiss();
    }

    return result.data ? result.data : result;
  }

  private setLoading(message: any) {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create();
      this.loading.setContent(message);
      this.loading.present();
      return;
    }

    this.loading.setContent(message);
  }

  private async buildLatLong(school: any) {
    const message = `
      <div>${this.currentState.nome}</div>
      <div>Importando ${++this.quantityByState} de ${this.total()}</div>
    `;
    this.setLoading(message);
    // busca latitude e longitude de acordo com o endereco
    let metadata = await this.geocodeService.resolveLatLong(school);
    
    // verifica se lat e long esta no mesmo estado
    if (metadata.status === GeocodeStatus.SUCCESS) {
      const lat = parseFloat(metadata.lat);
      const long = parseFloat(metadata.long);
      const isInside = await this.geocodeService.checkIsInsideState(lat, long, this.currentState);
      if (!isInside) {
        metadata.status = GeocodeStatus.INVALID_AREA;
      }
    }

    // salva lat e long resolvida
    await this.schoolService.saveSchoolLatLong(school.co_escola, school.sg_uf, metadata).toPromise();
    return metadata;
  }

}
